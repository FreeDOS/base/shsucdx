# SHSUCDX

CD-ROM and DVD-ROM extender

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SHSUCDX.LSM

<table>
<tr><td>title</td><td>SHSUCDX</td></tr>
<tr><td>version</td><td>3.09</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-09-02</td></tr>
<tr><td>description</td><td>CD-ROM and DVD-ROM extender</td></tr>
<tr><td>summary</td><td>CD-ROM and DVD-ROM extender</td></tr>
<tr><td>keywords</td><td>mscdex, shsucdex, fdcdex, cdrom, atapicdd, xcdrom</td></tr>
<tr><td>author</td><td>John H. McCoy</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jason Hood &lt;jadoxa -at- yahoo.com.au&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>http://adoxa.altervista.org/shsucdx/index.html</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/adoxa/shsucd</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/shsucdx/</td></tr>
<tr><td>platforms</td><td>DOS (NASM), FreeDOS</td></tr>
</table>
